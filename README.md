# libvcd #

**What is it?**
libvcd allows you to read and write 4-value VCD files, as specified in IEEE1364-2001. Cool, huh?

**Why do I care?**
You probably don't.

**Why should I care?**
* It's a really cool library! It has like objects and streams and everything.
* Oh, and it might not be abysmally slow. Although all things are relative.
* And it's reentrant!

## FAQ ##

*tumbleweed*

## Questions you thought you'd never ask ##

* Can I have both a `reader` and a `writer` operating on the same `vcd` object?

Yes! You can!

* Can I use all of the features of the 4-value VCD specification?

Yes! You can!

* Can I run a fun little demo application that parses an input VCD file and then reads it back to me, all the while emitting a truly irritating quantity of diagnostic information?

Yes! You can!

* Can I buy you a beer for implementing this awesome library for me?

Yes! You can!

## User Story ##

Hello! My name is Judith, and I am working on a highly exciting project, a gizmo that processes Verilog four-value logic in some magical way.

I want to be able to complete the project before the imminent and increasingly daunting deadline, so that I do not cause `<terrible life-altering consequence>` to befall me!

VCD seems like a great IO format! It's pretty ubiquitous and also very simple. Ideal! It also appears to be well specified in an open standard! Double ideal! There's also lots of software tooling that I have access to that speaks VCD! Triple ideal!

There must be an open-source VCD library, right? RIGHT? **RiGhT?!?!** *time passes*

Oh great! libvcd exists so I don't have to spend days writing a vcd parser and writer myself, expending what precious little terrible-consequence-avoidance-time I have left on something frankly really quite tedious!

The End.

## Future Work ##
aka. everything is terrible; things that are known to be broken, displeasing or otherwise unsatisfactory include:

* Escaped hierarchical net identifiers won't be found properly when being looked up. Someone should write a proper parser for hierarchical net names or something.
* Error handling & reporting is currently mediocre to disappointing, strenghening to poor in the north-east overnight.
* Documentation is non-existent.
* Dollar symbols in comments get mangled. Start states when lexing probably to blame.
* We currently do not output compressed vectors (emit full width only). Quelle horreur.
* Issues are currently being tracked via this list and `\\ FIXME`. If only there was some software for tracking issues with software.
