%skeleton "lalr1.cc"
%require "3.0.4"

%define api.namespace {libvcd}
%define parser_class_name {parser}

%define api.token.constructor
%define api.token.prefix {tok_}
%define api.value.type variant
%define parse.assert

%define parse.trace true

%defines

%code requires {
    #include "libvcd_tokens.h"
    #include "libvcd_tokwriter.h"

    namespace libvcd {
        class lexer;
    }
}

%param { libvcd::lexer& lexer }
%parse-param { libvcd::vcd_tokwriter& writer }

%locations
%initial-action
{
    @$.begin.filename = @$.end.filename = new std::string("filename");
}

%define parse.error verbose

%code {
    #include "libvcd_lexer.h"
    static libvcd::parser::symbol_type yylex(libvcd::lexer& lex) {
        return lex.lex();
    }
}

%token
    EOF 0      "EOF"
    KwComment
    KwDate
    KwVersion
    KwEndDefinitions
    KwUpscope
    KwScope
    KwTimescale
    KwVar
    KwDumpAll
    KwDumpOff
    KwDumpOn
    KwDumpVars
    IdxOpen     "["
    IdxClose    "]"
    IdxSep      ":"
    ScopeBegin
    ScopeFork
    ScopeFunction
    ScopeModule
    ScopeTask
    VartypeEvent
    VartypeInteger
    VartypeParameter
    VartypeReal
    VartypeReg
    VartypeSupply0
    VartypeSupply1
    VartypeTime
    VartypeTri
    VartypeTriand
    VartypeTrior
    VartypeTrireg
    VartypeTri0
    VartypeTri1
    VartypeWand
    VartypeWire
    VartypeWor
    End
    <std::string> Text
    <std::string> SimpleIdent
    <std::string> EscapedIdent
    <std::string> SimTime
    <tok::Decimal> Decimal
    <tok::Scalar> ScalarValue
    <tok::Binary> BinaryValue
    <tok::Real> RealValue
    <tok::Decimal> TimeVal
    <std::string> TimeUnit
    <std::string> NetId
    ;

%type <tok::value_tok> value;
%type <tok::value_change> value_change;
%type <std::vector<tok::value_change>> value_change_list;
%type <tok::dump> dumpall_block;
%type <tok::dump> dumpoff_block;
%type <tok::dump> dumpon_block;
%type <tok::dump> dumpvars_block;
%type <tok::OptionalDecimal> opt_range;
%type <tok::index> index;
%type <tok::index> opt_idx;
%type <var_type> var_type;
%type <tok::trim_string> ident;
%type <scope_type> scope_type;

%start vcd
%%

vcd:    decl_cmd_list enddefinitions_block simul_cmd_list;

decl_cmd_list:  decl_cmd decl_cmd_list
                | decl_cmd;

decl_cmd:       comment_block
                | date_block
                | scope_block
                | upscope_block
                | var_block
                | version_block
                | timescale_block;

simul_cmd_list: simul_cmd simul_cmd_list
                | ;

simul_cmd:      comment_block
                | dumpall_block     { writer.dump($1); } 
                | dumpoff_block     { writer.dump($1); } 
                | dumpon_block      { writer.dump($1); } 
                | dumpvars_block    { writer.dump($1); } 
                | SimTime           { writer.sim_time($1); }
                | value_change      { writer.change($1); };

enddefinitions_block: KwEndDefinitions End
                        { writer.endDefs(); };

comment_block: KwComment { writer.comment_start(); }
                    comment_text End { writer.comment_end(); };

comment_text:   Text comment_text { writer.comment_text($1); }
                | Text            { writer.comment_text($1); };

date_block: KwDate Text End
                { writer.setDate($2); };

scope_block: KwScope scope_type ident End
                { writer.enterScope($2, $3); };

scope_type: ScopeBegin          { $$ = scope_type::BLK_BEGIN; } 
            | ScopeFork         { $$ = scope_type::FORK; } 
            | ScopeFunction     { $$ = scope_type::FUNCTION; } 
            | ScopeModule       { $$ = scope_type::MODULE; } 
            | ScopeTask         { $$ = scope_type::TASK; } ;

ident:  SimpleIdent         { $$ = $1; }
        | EscapedIdent      { $$ = $1; };

upscope_block: KwUpscope End
                { writer.upscope(); };

timescale_block: KwTimescale TimeVal TimeUnit End
               { writer.timescale($2, $3); };

var_block: KwVar var_type Decimal NetId ident opt_idx End
                { writer.var($2, $3, $4, $5, $6); };

var_type:   VartypeEvent            { $$ = var_type::EVENT; }
            | VartypeInteger        { $$ = var_type::INTEGER; } 
            | VartypeParameter      { $$ = var_type::PARAMETER; } 
            | VartypeReal           { $$ = var_type::REAL; } 
            | VartypeReg            { $$ = var_type::REG; } 
            | VartypeSupply0        { $$ = var_type::SUPPLY0; } 
            | VartypeSupply1        { $$ = var_type::SUPPLY1; } 
            | VartypeTime           { $$ = var_type::TIME; } 
            | VartypeTri            { $$ = var_type::TRI; } 
            | VartypeTriand         { $$ = var_type::TRIAND; } 
            | VartypeTrior          { $$ = var_type::TRIOR; } 
            | VartypeTrireg         { $$ = var_type::TRIREG; } 
            | VartypeTri0           { $$ = var_type::TRI0; } 
            | VartypeTri1           { $$ = var_type::TRI1; } 
            | VartypeWand           { $$ = var_type::WAND; } 
            | VartypeWire           { $$ = var_type::WIRE; } 
            | VartypeWor            { $$ = var_type::WOR; } ;

opt_idx:    index   { $$ = $1; }
            |       { $$ = tok::index::missing(); };

index:      IdxOpen Decimal opt_range IdxClose
                            { $$ = tok::index($2, $3); };

opt_range:  IdxSep Decimal { $$ = $2; }
            |              { $$ = tok::OptionalDecimal::missing(); };

version_block: KwVersion Text End
                { writer.setVersion($2); };

dumpall_block: KwDumpAll value_change_list End
                { $$ = tok::dump($2, dump_type::ALL); };

dumpoff_block: KwDumpOff value_change_list End
                { $$ = tok::dump($2, dump_type::OFF); };

dumpon_block: KwDumpOn value_change_list End
                { $$ = tok::dump($2, dump_type::ON); };

dumpvars_block: KwDumpVars value_change_list End
                { $$ = tok::dump($2, dump_type::VARS); };

value_change_list:  value_change value_change_list
                        { $$ = $2; $$.push_back($1); }
                    | value_change
                        { $$ = std::vector<tok::value_change>();
                            $$.push_back($1); }

value_change: value NetId
                { $$ = tok::value_change($1, $2); }

value:  ScalarValue     { $$ = $1; }
        | RealValue     { $$ = $1; }
        | BinaryValue   { $$ = $1; };
%%

void libvcd::parser::error(const location_type& loc,
                            const std::string& msg) {
    // It's all gone pete tong
    // TODO
    std::cout << "E: " << msg << std::endl;
}
