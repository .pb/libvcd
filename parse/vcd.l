%option noyywrap
%option nodefault
%option reentrant
%option yylineno

%option outfile="parse/vcd.l.cpp"
%option header="parse/vcd.l.h"
%option prefix="libvcd_lex"

%option warn
%option debug

%{

#include "vcd.tab.hh"
#include "libvcd_lexer.h"
#include "libvcd_tokens.h"

using namespace libvcd;
%}

%x KW_TEXT
%x KW_VOID
%x KW_SCOPE
%x KW_TIME
%x KW_VAR
%x KW_VAR_T
%x KW_VAR_REF
%x KW_DUMP_VAL
%x KW_DUMP_ID
%x VC

ws [ \t\n]
end \$end

netident        [!-~]+
decnum          [0-9]+
dec_decimal     \.{decnum}+
exp             [eE][+\-]{decnum}+
simple_ident    [a-zA-Z_][a-zA-Z0-9_$]*

scalar [01xzXZ]
binary [bB]{scalar}+
real   [rR][decnum]+{dec_decimal}?{exp}?

%%

%{
    VCD_LEX_STEP
%}

#{decnum}               { return parser::make_SimTime(yytext, loc); }

\$date{ws}*             { BEGIN(KW_TEXT); return parser::make_KwDate(loc); }
\$version{ws}*          { BEGIN(KW_TEXT); return parser::make_KwVersion(loc); }
\$comment               { BEGIN(KW_TEXT); return parser::make_KwComment(loc); }
<KW_TEXT>[^$]+          { return parser::make_Text(yytext, loc); }
<KW_TEXT>\$end          { BEGIN(INITIAL); return parser::make_End(loc); }
<KW_TEXT>\$             { return parser::make_Text(yytext, loc); }

\$enddefinitions        { BEGIN(KW_VOID); return parser::make_KwEndDefinitions(loc); }
\$upscope               { BEGIN(KW_VOID); return parser::make_KwUpscope(loc); }
<KW_VOID>{ws}+          { /* Discard */ }
<KW_VOID>{end}          { BEGIN(INITIAL); return parser::make_End(loc); }

\$scope{ws}+            { BEGIN(KW_SCOPE); return parser::make_KwScope(loc); }
<KW_SCOPE>begin         { return parser::make_ScopeBegin(loc); }
<KW_SCOPE>fork          { return parser::make_ScopeFork(loc); }
<KW_SCOPE>function      { return parser::make_ScopeFunction(loc); }
<KW_SCOPE>module        { return parser::make_ScopeModule(loc); }
<KW_SCOPE>task          { return parser::make_ScopeTask(loc); }
<KW_SCOPE>{end}         { BEGIN(INITIAL); return parser::make_End(loc); }
<KW_SCOPE>{ws}+         { /* Discard */ }
<KW_SCOPE>[^\\]{simple_ident} { return parser::make_SimpleIdent(yytext, loc); }
<KW_SCOPE>\\[^ \t\n]+{ws}     { return parser::make_EscapedIdent(yytext, loc); }

\$timescale{ws}+        { BEGIN(KW_TIME); return parser::make_KwTimescale(loc); }
<KW_TIME>[0-9]+         { return parser::make_TimeVal(yytext, loc); }
<KW_TIME>[munpf]?s      { return parser::make_TimeUnit(yytext, loc); }
<KW_TIME>{ws}+          { /* Discard */ }
<KW_TIME>{end}          { BEGIN(INITIAL); return parser::make_End(loc); }

\$var{ws}+              { BEGIN(KW_VAR_T); return parser::make_KwVar(loc); }
<KW_VAR_T>event         { return parser::make_VartypeEvent(loc); }
<KW_VAR_T>integer       { return parser::make_VartypeInteger(loc); }
<KW_VAR_T>parameter     { return parser::make_VartypeParameter(loc); }
<KW_VAR_T>real          { return parser::make_VartypeReal(loc); }
<KW_VAR_T>reg           { return parser::make_VartypeReg(loc); }
<KW_VAR_T>supply0       { return parser::make_VartypeSupply0(loc); }
<KW_VAR_T>supply1       { return parser::make_VartypeSupply1(loc); }
<KW_VAR_T>time          { return parser::make_VartypeTime(loc); }
<KW_VAR_T>tri           { return parser::make_VartypeTri(loc); }
<KW_VAR_T>triand        { return parser::make_VartypeTriand(loc); }
<KW_VAR_T>trior         { return parser::make_VartypeTrior(loc); }
<KW_VAR_T>trireg        { return parser::make_VartypeTrireg(loc); }
<KW_VAR_T>tri0          { return parser::make_VartypeTri0(loc); }
<KW_VAR_T>tri1          { return parser::make_VartypeTri1(loc); }
<KW_VAR_T>wand          { return parser::make_VartypeWand(loc); }
<KW_VAR_T>wire          { return parser::make_VartypeWire(loc); }
<KW_VAR_T>wor           { return parser::make_VartypeWor(loc); }
<KW_VAR_T>{decnum}      { BEGIN(KW_VAR); return parser::make_Decimal(yytext, loc); }
<KW_VAR_T>{ws}+         { /* Discard */ }

<KW_VAR>{netident}              { BEGIN(KW_VAR_REF); return parser::make_NetId(yytext, loc); }
<KW_VAR>{ws}+                   { /* Discard */ }
<KW_VAR_REF>{end}               { BEGIN(INITIAL); return parser::make_End(loc); }
<KW_VAR_REF>[^\\]{simple_ident} { return parser::make_SimpleIdent(yytext, loc); }
<KW_VAR_REF>\\[^ \t\n]+{ws}     { return parser::make_EscapedIdent(yytext, loc); }
<KW_VAR_REF>\[                  { return parser::make_IdxOpen(loc); }
<KW_VAR_REF>\]                  { return parser::make_IdxClose(loc); }
<KW_VAR_REF>:                   { return parser::make_IdxSep(loc); }
<KW_VAR_REF>{decnum}            { return parser::make_Decimal(yytext, loc); }
<KW_VAR_REF>{ws}+               { /* Discard */ }

\$dumpall               { BEGIN(KW_DUMP_VAL); return parser::make_KwDumpAll(loc); }
\$dumpoff               { BEGIN(KW_DUMP_VAL); return parser::make_KwDumpOff(loc); }
\$dumpon                { BEGIN(KW_DUMP_VAL); return parser::make_KwDumpOn(loc); }
\$dumpvars              { BEGIN(KW_DUMP_VAL); return parser::make_KwDumpVars(loc); }

<KW_DUMP_VAL>{scalar}   { BEGIN(KW_DUMP_ID); return parser::make_ScalarValue(yytext, loc); }
<KW_DUMP_VAL>{binary}   { BEGIN(KW_DUMP_ID); return parser::make_BinaryValue(yytext, loc); }
<KW_DUMP_VAL>{real}     { BEGIN(KW_DUMP_ID); return parser::make_RealValue(yytext, loc); }
<KW_DUMP_VAL>{ws}+      { /* Discard */ }
<KW_DUMP_VAL>{end}      { BEGIN(INITIAL); return parser::make_End(loc); }
<KW_DUMP_ID>{netident}  { BEGIN(KW_DUMP_VAL); return parser::make_NetId(yytext, loc); }
<KW_DUMP_ID>{ws}+       { /* Discard */ }

<INITIAL>{scalar}       { BEGIN(VC); return parser::make_ScalarValue(yytext, loc); }
<INITIAL>{binary}       { BEGIN(VC); return parser::make_BinaryValue(yytext, loc); }
<INITIAL>{real}         { BEGIN(VC); return parser::make_RealValue(yytext, loc); }
<VC>{netident}          { BEGIN(INITIAL); return parser::make_NetId(yytext, loc); }
<VC>{ws}+               { /* Discard */ }

{ws}+ { /* Discard */ }

<INITIAL,KW_TEXT,KW_VOID,KW_SCOPE,KW_TIME,KW_DUMP_VAL,KW_DUMP_ID,VC>. {
        //TODO: Just do something about this abomination
        printf("!!!! Unexpected input: %s (line %d)\n", yytext, yylineno);
}

<KW_VAR_T,KW_VAR_REF,KW_VAR>. {
        //TODO: Just do something about this abomination
        printf("!!!! <KW_VAR*> Unexpected input: %s (line %d)\n", yytext, yylineno);
}

%%
