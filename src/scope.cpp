#include "libvcd_vcd.h"

namespace libvcd {
    scope::scope(scope* parent,
                    scope_type type,
                    const std::string& name):
        name_(name),
        type_(type),
        parent_(parent),
        nets_(),
        children_() {
    }

    scope& scope::addChild(scope_type type,
                            const std::string& name) {
        children_.emplace_back(scope(this, type, name));

        return *children_.rbegin();
    }

    void scope::addNet(net::id_t net) {
        nets_.push_back(net);
    }

    scope_type scope::type() const {
        return type_;
    }

    scope* scope::childWithName(const std::string& name) {
        for (scope& child : children_) {
            if (child.name() == name)
                return &child;
        }

        return nullptr;
    }

    net* scope::netWithName(const std::string& name, const netmap& map) {
        for (net::id_t n : nets_) {
            if (map.getNet(n)->name() == name)
                return map.getNet(n);
        }

        return nullptr;
    }

    const std::string& scope::name() const {
        return name_;
    }

    std::vector<const scope*> scope::children() const {
        std::vector<const scope*> retvec;

        for (const scope& scp : children_)
            retvec.push_back(&scp);

        return retvec;
    }

    const std::vector<net::id_t>& scope::nets() const {
        return nets_;
    }

    scope* scope::parent() {
        return parent_;
    }
}
