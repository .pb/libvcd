#include "libvcd_lexer.h"
#include "vcd.l.h"

#include <iostream>

namespace libvcd {
    lexer::lexer() {
        if (libvcd_lexlex_init(&yyscanner))
            throw lexer_init_error();
    }

    void lexer::setSourceFile(FILE* fp) {
        libvcd_lexset_in(fp, yyscanner);
    }

    void lexer::setSourceBuffer(const char* buf, int len) {
        libvcd_lex_scan_bytes(buf, len, yyscanner);
    }

    lexer::~lexer() {
        libvcd_lexlex_destroy(yyscanner);
    }
}
