#include "libvcd_types.h"

namespace libvcd {
    std::ostream& operator<<(std::ostream& os, const net_type& net) {
        os << "net_type::";
        switch (net) {
            case net_type::SCALAR:
                return os << "SCALAR";

            case net_type::VECTOR:
                return os << "VECTOR";

            case net_type::REAL:
                return os << "REAL";
        }
    }

    std::ostream& operator<<(std::ostream& os, const var_type& net) {
        switch (net) {
        case var_type::EVENT:
            return os << "event";

        case var_type::INTEGER:
            return os << "integer";

        case var_type::PARAMETER:
            return os << "parameter";

        case var_type::REAL:
            return os << "real";

        case var_type::REG:
            return os << "reg";

        case var_type::SUPPLY0:
            return os << "supply0";

        case var_type::SUPPLY1:
            return os << "supply1";

        case var_type::TIME:
            return os << "time";

        case var_type::TRI:
            return os << "tri";

        case var_type::TRIAND:
            return os << "triand";

        case var_type::TRIOR:
            return os << "trior";

        case var_type::TRIREG:
            return os << "trireg";

        case var_type::TRI0:
            return os << "tri0";

        case var_type::TRI1:
            return os << "tri1";

        case var_type::WAND:
            return os << "wand";

        case var_type::WIRE:
            return os << "wire";

        case var_type::WOR:
            return os << "wor";

        default:
            return os << "???"; // FIXME: Exception?
        }
    }
}
