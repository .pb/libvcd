#include "libvcd_vcd.h"

#include <sstream>

namespace libvcd {
    net::id_t netmap::addNet(net net_ref) {
        return addMap(std::move(net_ref), generateIdent());
    }

    net::id_t netmap::addMap(net net_ref, const net::ident_t& ident) {
        net::id_t newId = nets_.size();
        net_ref.setId(newId);
        nets_.push_back(std::move(net_ref));
        nets_[newId].setIdent(ident);
        ident_map_.emplace(ident, newId);

        size_t type_index;

        switch (nets_[newId].netType()) {
            case net_type::SCALAR:
                type_index = scalar_count_++;
                break;

            case net_type::VECTOR:
                type_index = vector_count_++;
                break;

            case net_type::REAL:
                type_index = real_count_++;
                break;
        }

        type_index_lookup_.push_back(type_index);

        return newId;
    }

    const net* netmap::getNet(const net::ident_t& ident) const {
        auto it = ident_map_.find(ident);
        if (it == ident_map_.end())
            return nullptr;

        return &nets_[it->second];
    }

    net* netmap::getNet(const net::id_t& id) const {
        if (id < 0 || id >= nets_.size())
            return nullptr;

        return const_cast<net*>(&nets_[id]);
    }

    const size_t& netmap::scalarCount() const {
        return scalar_count_;
    }

    const size_t& netmap::vectorCount() const {
        return vector_count_;
    }

    const size_t& netmap::realCount() const {
        return real_count_;
    }

    const size_t& netmap::getTypeIndex(const net::id_t& id) const {
        return type_index_lookup_[id];
    }

    const net::ident_t netmap::generateIdent() {
        constexpr char maxIdent = '~';
        constexpr char minIdent = '!';
        constexpr char identRng = maxIdent - minIdent;

        do {
            std::vector<char> desc;

            int work_idx = ident_idx_++;

            do {
                desc.push_back((char) (minIdent + (work_idx % identRng)));
                work_idx /= identRng;
            } while (work_idx);

            std::stringstream ss;

            for (auto it = desc.rbegin(); it != desc.rend(); it++)
                ss << *it;

            const std::string& ident = ss.str();

            /* Ensure generated ident isn't already in use
             * (e.g. manually added mappings) */
            if (ident_map_.find(ident) == ident_map_.end())
                return ident;
        } while (true);
    }

    const std::vector<net>& netmap::getNets() const {
        return nets_;
    }
}
