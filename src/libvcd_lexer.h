#ifndef LIBVCD_LEXER_H
#define LIBVCD_LEXER_H

#define YY_TYPEDEF_YY_SCANNER_T
typedef void* yyscan_t;

#define yyterminate() return parser::make_EOF(loc);

#define YY_DECL libvcd::parser::symbol_type libvcd::lexer::lex()
#define YY_USER_ACTION loc.columns(yyleng);
#define VCD_LEX_STEP loc.step();

#include "vcd.tab.hh"
#include "location.hh"

#include <sstream>
#include <cstring>

namespace libvcd {
    class token;
    class parser;

    class lexer {
    public:
        lexer();
        ~lexer();
        lexer operator=(lexer&) = delete;
        lexer(lexer&) = delete;

        // Implemented in the flex output file
        libvcd::parser::symbol_type lex();

        void setSourceFile(FILE* fp);
        void setSourceBuffer(const char* buf, int len);

    private:
        yyscan_t yyscanner;
        location loc;
    };

    class lexer_init_error : std::runtime_error {
        public:
            lexer_init_error():
                std::runtime_error("Lexer init failed") {
            }
    };
}

#endif
