#ifndef LIBVCD_TOKENS_H
#define LIBVCD_TOKENS_H

#include <string>

#include "libvcd_vval.h"
#include "libvcd_types.h"

namespace libvcd {
    namespace tok {
        class TokBase {
        protected:
            TokBase() {}
            TokBase(const char* strVal):
                val_(strVal) {
            }

            std::string val_;
        };

        class Decimal : public TokBase {
        public:
            Decimal() {}
            Decimal(const char* value):
                TokBase(value) {
            };

            int val() const;
            const char* innerval() const;
        };

        class OptionalDecimal : public Decimal {
        public:
            OptionalDecimal();
            OptionalDecimal(const char* value);

            static OptionalDecimal missing();
            OptionalDecimal operator=(const Decimal& decimal);
            bool present() const;

        protected:
            bool missing_;
        };

        class Scalar : public TokBase {
        public:
            Scalar() {}
            Scalar(const char* value):
                TokBase(value) {
            }

            VVal val() const;
        };

        class Binary : public TokBase {
        public:
            Binary() {}
            Binary(const char* value):
                TokBase(value) {
            }

            Binary& operator=(const Binary& bin) {
                val_ = bin.val_;
                return *this;
            }

            VVec val(unsigned int width) const;
        };

        class Real : TokBase {
        public:
            Real() {}
            Real(const char* value):
                TokBase(value) {
            }

            float val() const;
        };

        class value_change;

        class value_tok {
        public:
            value_tok& operator=(Real& val);
            value_tok& operator=(Scalar& val);
            value_tok& operator=(Binary& val);

            friend value_change;

        protected:
            net_type type_;
            VVal val_;
            Binary vec_;
            float real_;

            void checkType(net_type t) const;
        };

        class value_change {
        public:
            value_change();
            value_change(const value_tok& v, const std::string& NetId);
            const std::string& ident() const;

            const VVal& scalarVal() const;
            VVec vectorVal(unsigned int width) const;
            const float& realVal() const;

        protected:
            value_tok valtok_;
            std::string ident_;
        };

        class dump {
            public:
                dump();
                dump(const std::vector<value_change>& vcList,
                     dump_type type);
                dump_type type() const;
                const std::vector<value_change>& values() const;
            protected:
                dump_type type_;
                std::vector<value_change> changes_;
        };

        class index {
        public:
            index();
            index(Decimal from, OptionalDecimal to);
            static index missing();
            bool present() const;
            index_t val() const;
        protected:
            int from_;
            int to_;
            bool has_to_;
            bool missing_;
        };

        class trim_string {
        public:
            trim_string();
            trim_string(const std::string& str);
            trim_string& operator=(const std::string& str);

            operator std::string&();
            operator const char*();

        protected:
            void updatestr(const std::string& str);
            std::string str_;
        };
    }
}

#endif
