#ifndef LIBVCD_TYPES_H
#define LIBVCD_TYPES_H

#include <ostream>

namespace libvcd {
    enum class dump_type {
        NONE,
        ALL,
        ON,
        OFF,
        VARS
    };

    std::ostream& operator<<(std::ostream& os, const dump_type& typ);

    enum class time_unit {
        SEC,
        MILLI,
        MICRO,
        NANO,
        PICO,
        FEMTO
    };

    std::ostream& operator<<(std::ostream& os, const time_unit& unit);

    struct timescale {
        int val;
        time_unit unit;
        bool set = false;
    };

    enum class scope_type {
        MODULE,
        TASK,
        FUNCTION,
        BLK_BEGIN,
        FORK,
        ROOT
    };

    std::ostream& operator<<(std::ostream& os, const scope_type& scope);

    enum class net_type {
        SCALAR,
        VECTOR,
        REAL
    };

    std::ostream& operator<<(std::ostream& os, const net_type& net);

    enum class var_type {
        EVENT,
        INTEGER,
        PARAMETER,
        REAL,
        REG,
        SUPPLY0,
        SUPPLY1,
        TIME,
        TRI,
        TRIAND,
        TRIOR,
        TRIREG,
        TRI0,
        TRI1,
        WAND,
        WIRE,
        WOR
    };

    std::ostream& operator<<(std::ostream& os, const var_type& net);

    constexpr net_type getNetType(var_type t, int width) {
        switch (t) {
            case var_type::EVENT:
                return net_type::SCALAR;

            case var_type::INTEGER:
                return net_type::VECTOR;

            case var_type::PARAMETER:
            case var_type::REAL:
            case var_type::TIME:
                return net_type::REAL;

            case var_type::REG:
            case var_type::SUPPLY0:
            case var_type::SUPPLY1:
            case var_type::TRI:
            case var_type::TRIAND:
            case var_type::TRIOR:
            case var_type::TRIREG:
            case var_type::TRI0:
            case var_type::TRI1:
            case var_type::WAND:
            case var_type::WIRE:
            case var_type::WOR:
            default:
                if (width > 1)
                    return net_type::VECTOR;

                return net_type::SCALAR;
        }
    }

    struct index_t {
        bool exists;
        bool has_to;
        int from;
        int to;

        static index_t missing() {
            index_t idx;
            idx.exists = false;
            idx.has_to = false;
            idx.from = -1;
            idx.to = -1;

            return idx;
        }
    };
}

#endif
