#include "libvcd_writer.h"
#include "libvcd_tokwriter.h"
#include "libvcd_version.h"

#include <sstream>
#include <stdexcept>
#include <chrono>

namespace libvcd {
    vcd_writer::vcd_writer(vcd& vcdump):
        writing_definitions_(true),
        vcd_(vcdump),
        seek_idx_(-1),
        current_scope_(&vcdump.root_) {
            setVersion("vcd_writer");
            setDate();
    }

    time_t vcd_writer::time() const {
        if (seek_idx_ < 0)
            return -1;

        return vcd_.entries_[seek_idx_].time();
    }

    void vcd_writer::setCurrentType(dump_type typ) {
        if (seek_idx_ < 0)
            return;

        vcd_.entries_[seek_idx_].setType(typ);
    }

    void vcd_writer::comment(const std::string& comment) {
        if (writing_definitions_) {
            if (vcd_.file_comment_.size()) {
                // FIXME: This is hacktastic. Bleugh.
                std::stringstream ss;

                ss << vcd_.file_comment_
                    << "$end\n$comment\n"
                    << comment;

                vcd_.file_comment_ = ss.str();
            } else {
                vcd_.file_comment_ = comment;
            }
        } else {
            size_t commentIdx = vcd_.comments_.size();
            vcd_.comments_.push_back(comment);
            vcd_.comment_indexes_.emplace(seek_idx_, std::vector<int>());
            vcd_.comment_indexes_.find(seek_idx_)->second.push_back(commentIdx);
        }
    }

    void vcd_writer::time(time_t t) {
        seek_idx_++;
        vcd_.entries_.emplace_back(t, dump_type::NONE);
    }

    void vcd_writer::enterScope(scope_type sc_type, const std::string& ident) {
        scope* newScope = current_scope_->childWithName(ident);

        if (newScope == nullptr)
            current_scope_ = &current_scope_->addChild(sc_type, ident);
        else
            current_scope_ = newScope;
    }

    void vcd_writer::upscope() {
        current_scope_ = current_scope_->parent();

        if (current_scope_ == nullptr)
            throw std::runtime_error("Cannot $upscope from unscoped!");
    }

    void vcd_writer::setVersion(const std::string& str) {
        std::stringstream ss;

        ss << LIBVCD_VERSION << " " << str;

        vcd_.version_ = ss.str();
    }

    void vcd_writer::setFileComment(const std::string& comment) {
        vcd_.file_comment_ = comment;
    }

    void vcd_writer::setDate() {
        auto now = std::chrono::system_clock::now();
        std::time_t t = std::chrono::system_clock::to_time_t(now);
        setDate(std::ctime(&t));
    }

    void vcd_writer::setDate(const std::string& date) {
        vcd_.file_date_ = date;
    }

    void vcd_writer::setTimescale(int value, time_unit unit) {
        vcd_.timescale_.val = value;
        vcd_.timescale_.unit = unit;
        vcd_.timescale_.set = true;
    }

    vcd& vcd_writer::get() {
        return vcd_;
    }

    void vcd_writer::scalar(net::id_t net, const VVal& val) {
        vcd_.data_.scalars().emplace_back(net, val);
        vcd_.entries_[seek_idx_].indexes().scalar(vcd_.data_.scalars().size() - 1);
    }

    void vcd_writer::vector(net::id_t net, const VVec& val) {
        vcd_.data_.vectors().emplace_back(net, val);
        vcd_.entries_[seek_idx_].indexes().vector(vcd_.data_.vectors().size() - 1);
    }

    void vcd_writer::real(net::id_t net, float val) {
        vcd_.data_.reals().emplace_back(net, val);
        vcd_.entries_[seek_idx_].indexes().real(vcd_.data_.reals().size() - 1);
    }

    void vcd_writer::endDefs() {
        writing_definitions_ = false;
    }

    void vcd_tokwriter::date(const std::string& text) {
        setDate(text);
    }

    void vcd_tokwriter::timescale(tok::Decimal& timeVal, const std::string& unit_str) {
        auto getTime = [&](char unitChar) {
            switch (unitChar) {
                case 'm':
                    return time_unit::MILLI;

                case 'u':
                    return time_unit::MICRO;

                case 'n':
                    return time_unit::NANO;

                case 'p':
                    return time_unit::PICO;

                case 'f':
                    return time_unit::FEMTO;

                default:
                case 's':
                    return time_unit::SEC;
            }
        };

        setTimescale(timeVal.val(), getTime(unit_str.front()));
    }

    net::id_t vcd_writer::declScalar(var_type typ,
                                const std::string& netName) {
        net::id_t id = vcd_.map_.addNet(net(netName,
                                            typ,
                                            1,
                                            index_t::missing(),
                                            *current_scope_));
        current_scope_->addNet(id);
        return id;
    }

    void vcd_tokwriter::var(var_type typ,
                            tok::Decimal& size,
                            const std::string& netIdent,
                            const std::string& netName,
                            const tok::index& idx) {
        if (!writing_definitions_)
            throw std::runtime_error("Can't define after $enddefinitions!");
        // TODO: FIXUP netName to include index info
        // TODO: Validate that size matches indexing data
        index_t index = idx.val();
        net::id_t id = vcd_.map_.addMap(net(netName,
                                            typ,
                                            size.val(),
                                            index,
                                            *current_scope_),
                                        netIdent);
        current_scope_->addNet(id);
    }

    void vcd_tokwriter::change(const tok::value_change& change) {
        const net* n = vcd_.map_.getNet(change.ident());

        if (n == nullptr)
            throw std::runtime_error("Change for unknown net");

        switch (n->netType()) {
            case net_type::SCALAR:
                vcd_.data_.scalars().emplace_back(n->id(), change.scalarVal());
                vcd_.entries_[seek_idx_].indexes().scalar(vcd_.data_.scalars().size() - 1);
                break;

            case net_type::VECTOR:
                vcd_.data_.vectors().emplace_back(n->id(), change.vectorVal(n->vec_width()));
                vcd_.entries_[seek_idx_].indexes().vector(vcd_.data_.vectors().size() - 1);
                break;

            case net_type::REAL:
                vcd_.data_.reals().emplace_back(n->id(), change.realVal());
                vcd_.entries_[seek_idx_].indexes().real(vcd_.data_.reals().size() - 1);
                break;
        }
    }

    void vcd_tokwriter::sim_time(const std::string& timeStr) {
        time_t t;
        std::stringstream ss(timeStr.substr(1)); // Strip leading #
        ss >> t;

        time(t);
    }

    void vcd_tokwriter::dump(const tok::dump& dump_tok) {
        vcd_.entries_[seek_idx_].setType(dump_tok.type());

        for (const tok::value_change& ch : dump_tok.values()) {
            change(ch);
        }
    }

    void vcd_tokwriter::comment_start() {
        comment_buffer_.clear();
        comment_buffer_.str(std::string());
    }

    void vcd_tokwriter::comment_text(const std::string& text) {
        comment_buffer_ << text;
    }

    void vcd_tokwriter::comment_end() {
        comment(comment_buffer_.str());
    }
}
