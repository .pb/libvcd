#include "libvcd_printer.h"

#include<iterator>

namespace libvcd {
    std::ostream& operator<<(std::ostream& os, const vcd& vcd) {
        vcd_reader r(vcd);
        vcd_printer p(vcd, r);

        return p(os);
    }

    vcd_printer::vcd_printer(const vcd& v, vcd_reader& r):
        v_(v), r_(r) {
    }

    std::ostream& vcd_printer::operator()(std::ostream& os) {
        if (v_.date().size())
            printDate(os);

        if (v_.version().size())
            printVersion(os);

        if (v_.fileComment().size())
            printFileComment(os);

        if (v_.timescale_.set)
            printTimescale(os);

        printVars(os);
        printBlock(os, "enddefinitions");
        printReader(os);

        return os;
    }

    void vcd_printer::printDate(std::ostream& os) {
        printBlock(os, "date", v_.date());
    }

    void vcd_printer::printVersion(std::ostream& os) {
        printBlock(os, "version", v_.version());
    }

    void vcd_printer::printFileComment(std::ostream& os) {
        printBlock(os, "comment", v_.fileComment());
    }

    void vcd_printer::printTimescale(std::ostream& os) {
        os << "$timescale " << v_.timescale_.val << " "
            << v_.timescale_.unit << std::endl
            << "$end" << std::endl;
    }

    void vcd_printer::printVars(std::ostream& os) {
        printVars(os, &v_.root_, false);
    }

    void vcd_printer::printVars(std::ostream& os, const scope* s, bool printThis) {
        if (printThis)
            os << "$scope " << s->type() << " " << s->name() << " $end" << std::endl;

        for (net::id_t nId : s->nets())
            printNet(os, v_.map_.getNet(nId));

        for (const scope* s : s->children())
            printVars(os, s);

        if (printThis)
            os << "$upscope $end" << std::endl;
    }

    void vcd_printer::printNet(std::ostream& os, const net* n) {
        os << "$var " << n->varType() << " " << (int) n->vec_width() << " "
            << n->ident() << " " << n->name() << " $end" << std::endl;
    }

    void vcd_printer::printReader(std::ostream& os) {
        printComments(os, r_.comments());
        time_t lastTime = -1;

        while (r_.seekNext()) {
            time_t newTime = r_.time();

            if (lastTime != newTime) {
                lastTime = newTime;
                printTime(os, newTime);
            }

            printComments(os, r_.comments());

            bool dump = false;

            if (r_.dumpType() != dump_type::NONE) {
                dump = true;
                os << "$dump" << r_.dumpType() << std::endl;
            }

            vcd_entry_data& d = const_cast<vcd_entry_data&>(v_.const_data());
            const vcd_entry_indexes& idx = v_.entries_[r_.seek_idx_].const_indexes();

            if (v_.map_.scalarCount()) { // Prevent construction if not exists
                auto idx_pair = getIndexPair(d.scalars(), idx.scalar_idx, idx.scalar_count);

                for (auto pair = idx_pair.first; pair != idx_pair.second; pair++)
                    os << pair->second << v_.map_.getNet(pair->first)->ident() << std::endl;
            }

            if (v_.map_.vectorCount()) { // Prevent construction if not exists
                auto idx_pair = getIndexPair(d.vectors(), idx.vector_idx, idx.vector_count);

                for (auto pair = idx_pair.first; pair != idx_pair.second; pair++)
                    os << pair->second << " " << v_.map_.getNet(pair->first)->ident() << std::endl;
            }

            if (v_.map_.realCount()) { // Prevent construction if not exists
                auto idx_pair = getIndexPair(d.reals(), idx.real_idx, idx.real_count);

                for (auto pair = idx_pair.first; pair != idx_pair.second; pair++)
                    os << pair->second << " " << v_.map_.getNet(pair->first)->ident() << std::endl;
            }

            if (dump)
                os << "$end" << std::endl;
        }
    }

    void vcd_printer::printTime(std::ostream& os, time_t time) {
        os << "#" << time << std::endl;
    }

    void vcd_printer::printComments(std::ostream& os, const std::vector<std::string>& comments) {
        for (const std::string& commentStr : comments)
            printBlock(os, "comment", commentStr);
    }

    void vcd_printer::printBlock(std::ostream& os,
                                    const std::string& name,
                                    const std::string& contents) {
        os << "$" << name << std::endl
            << contents << std::endl
            << "$end" << std::endl;
    }

    std::ostream& operator<<(std::ostream& os, const time_unit& unit) {
        switch (unit) {
            case time_unit::SEC:
                return os << "s";

            case time_unit::MILLI:
                return os << "ms";

            case time_unit::MICRO:
                return os << "us";

            case time_unit::NANO:
                return os << "ns";

            case time_unit::PICO:
                return os << "ps";

            case time_unit::FEMTO:
                return os << "fs";
        }
    }

    std::ostream& operator<<(std::ostream& os, const scope_type& typ) {
        switch (typ) {
        case scope_type::MODULE:
            return os << "module";

        case scope_type::TASK:
            return os << "task";

        case scope_type::FUNCTION:
            return os << "function";

        case scope_type::BLK_BEGIN:
            return os << "begin";

        case scope_type::FORK:
            return os << "fork";

        default:
            return os << "???"; // FIXME Throw exception?
        }
    }

    std::ostream& operator<<(std::ostream& os, const dump_type& typ) {
        switch (typ) {
        case dump_type::ALL:
            return os << "all";

        case dump_type::ON:
            return os << "on";

        case dump_type::OFF:
            return os << "off";

        case dump_type::VARS:
            return os << "vars";

        default:
            return os << "???"; // FIXME Exception?
        }
    }
}
